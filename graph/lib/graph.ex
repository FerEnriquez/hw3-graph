defmodule Graph do
  def compute() do 
    
    File.read!("example.csv")
    |>String.split("\n")
    |>Enum.map_every(1,fn x -> {String.split(x, ",")
      |>Enum.at(1)} end)
    |>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)
 #File.read!("example.csv")|>String.split("\n") |>Enum.map_every(1,fn x -> {String.split(x, ",")|>Enum.at(1)} end)|>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)
   
 
 #OBTIENE TODAS LAS ACTIVIDADES
    File.stream!("example.csv")
    |>Stream.drop(1)
    |>Stream.map(fn s -> String.trim(s)
      |> String.split(",")
      |> Enum.at(1) end) 
    |>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)
#File.stream!("example.csv")|>Stream.drop(1)|>Stream.map(fn s -> String.trim(s)|> String.split(",")|> Enum.at(1) end) |>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)

#OBTIENE CUANTAS ACTIVIDADES HAY POR CASO
    File.stream!("example.csv")
    |>Stream.drop(1)
    |>Stream.map(fn s -> String.trim(s)
      |> String.split(",")
      |> Enum.take(1) end)
    |>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)
    |>Enum.sort()
#File.stream!("example.csv")|>Stream.drop(1)|>Stream.map(fn s -> String.trim(s)|> String.split(",")|> Enum.take(1) end)|>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)|>Enum.sort()
  
#DE UNA ACTIVIDAD A OTRA  
    File.stream!("example.csv")
    |>Stream.drop(1)
    |>Stream.map(fn s -> String.trim(s)
      |> String.split(",")
      |> Enum.at(1) end) 
    |> Enum.chunk_every(2)
    |>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)
#File.stream!("example.csv")|>Stream.drop(1)|>Stream.map(fn s -> String.trim(s)|> String.split(",")|> Enum.at(1) end) |> Enum.chunk_every(2)|>Enum.reduce(%{}, fn word, map -> Map.update(map, word, 1, & &1 + 1) end)

  end
end


